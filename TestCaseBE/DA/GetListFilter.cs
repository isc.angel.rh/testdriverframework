﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.BE.DA
{
    public interface GetListFilter<out R, in F>
    {
        IEnumerable<R> Get(F Filter);
    }
}
