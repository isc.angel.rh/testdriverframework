﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.BE.DA
{
    interface GetList<out R>
    {
        IEnumerable<R> Get();
    }
}
