﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestCase.Model.Context;
using System.Data.Entity.Migrations;

namespace TestCase.BE.DA
{
    public class BaseDa:IDisposable
    {

        protected TestCaseContext db
        {
            get
            {
                return ConnectionManager.GetContext();
            }
        }

        public void Add<T>(T item) where T : class
        {
            db.Set<T>().Add(item);
            db.SaveChanges();
        }

        public DbSet<T> GetSet<T>() where T : class
        {
            return db.Set<T>();
        }

        public async Task<int> AddAsync<T>(T item) where T : class
        {
            db.Set<T>().Add(item);
            return await db.SaveChangesAsync();
        }

        public void Update<T>(Func<T, bool> searchExpression, Func<T, T> parse) where T : class
        {
            FindAndUpdateItem(searchExpression, parse);
            db.SaveChanges();
        }

        public async void UpdateAsync<T>(Func<T, bool> searchExpression, Func<T, T> parse) where T : class
        {
            FindAndUpdateItem(searchExpression, parse);
            await db.SaveChangesAsync();
        }

        //public async Task<int> UpdateAsync<T>(T item, int? id) where T : class
        //{
        //    db.Set<T>().AddOrUpdate(item);
        //    return await db.SaveChangesAsync();
        //}
        public async Task<int> UpdateAsync<T>(T item) where T : class
        {
            //db.Set<T>().AddOrUpdate(item);
            return await db.SaveChangesAsync();
        }

        private void FindAndUpdateItem<T>(Func<T, bool> searchExpression, Func<T, T> parse) where T : class
        {
            var dbItem = db.Set<T>()
                            .ToList()
                            .Where(w => searchExpression(w))
                            .FirstOrDefault();
            if (dbItem == null)
            {
                throw new Exception("Item to update was not found");
            }
            dbItem = parse(dbItem);
        }

        public void Delete<T>(T item) where T : class
        {
            db.Set<T>().Attach(item);
            db.Set<T>().Remove(item);
            db.SaveChanges();
        }

        public async Task<int> DeleteAsync<T>(T item) where T : class
        {
            db.Set<T>().Attach(item);
            db.Set<T>().Remove(item);
            return await db.SaveChangesAsync();
        }

        public T Get<T>(Func<T, bool> searchExpression) where T : class
        {
            var dbItem = db.Set<T>()
                            .ToList()
                            .Where(w => searchExpression(w))
                            .FirstOrDefault();
            return dbItem;
        }

        public T Get<T>(int? id) where T : class
        {
            var dbItem = db.Set<T>().Find(id);
            return dbItem;
        }

        public T Get<T>(Guid id) where T : class
        {
            var dbItem = db.Set<T>().Find(id);
            return dbItem;
        }

        public async Task<T> GetAsync<T>(int? id) where T : class
        {
            var dbItem = await db.Set<T>().FindAsync(id);
            return dbItem;
        }
        public async Task<T> GetAsync<T>(Guid? id) where T : class
        {
            var dbItem = await db.Set<T>().FindAsync(id);
            return dbItem;
        }



        public T Get<T>(string id) where T : class
        {
            var dbItem = db.Set<T>().Find(id);
            return dbItem;
        }

        public List<T> GetAll<T>() where T : class
        {
            var dbItem = db.Set<T>()
                            .ToList();
            return dbItem;
        }

        public async Task<List<T>> GetAllAsync<T>() where T : class
        {
            return await db.Set<T>().ToListAsync();
        }
        public async Task<List<T>> GetAllAsync<T>(Func<T, bool> searchExpression) where T : class
        {
            var dbItem = await db.Set<T>()
                            .Where(w => searchExpression(w))
                            .ToListAsync();
            return dbItem;
        }

        public List<T> GetAll<T>(Func<T, bool> searchExpression) where T : class
        {
            var dbItem = db.Set<T>()
                            .ToList()
                            .Where(w => searchExpression(w))
                            .ToList();
            return dbItem;
        }

        public bool Exists<T>(int? id) where T : class
        {
            var dnItem = db.Set<T>().FirstOrDefault();
            return dnItem != null;
        }

        public void Dispose()
        {
            new ConnectionManager().Dispose();
        }
    }

}
