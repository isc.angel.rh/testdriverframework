﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TestCase.BE.Model.TestCase;

namespace TestCase.BE.DA.TestCase
{
    public class GetTestToExecute : BaseDa, GetListFilter<TestFromDb, TestCaseFilter>
    {
        public IEnumerable<TestFromDb> Get(TestCaseFilter Filter)
        {
            try
            {
                var list = from app in db.App
                           join ea in db.EnviromentApp on app.Id equals ea.AppId
                           join ev in db.Environment on ea.EnviromentId equals ev.Id
                           join tc in db.TestCase on app.Id equals tc.AppId
                           join st in db.Set on tc.Id equals st.TestCaseId
                           join eo in db.ExecuteOn on tc.Id equals eo.TestCaseId
                           join ex in db.Explorer on eo.ExplorerId equals ex.Id
                           where app.Key == Filter.App
                           && ea.Execute == Filter.ExecuteEnviroment
                           && st.Execute == Filter.ExecuteSet
                           && eo.Execute == true
                           select new TestFromDb()
                           {
                               App = app.Key,
                               Enviroment = ev.Description,
                               Link = ev.Link,
                               ExplorerKey = ex.Key,
                               Key = tc.Key,
                               Name = tc.Name,
                               Description = tc.Description,
                               Priority = tc.Priority,
                               Parallet = tc.Parallet,
                               Set = st,
                               Parameter = db.Parameter.Where(w=> w.SetId == st.Id)
                           };
                return list;
            }
            catch(Exception ex)
            {

            }
            return new List<TestFromDb>();
        }
    }
}
