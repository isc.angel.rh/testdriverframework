﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using TestCase.Model.Model;
using TestCase.Web.Base.Test.Builder;

namespace TestCase.BE.Model.TestCase
{
    public class TestFromDb
    {
        public string App { get; set; }
        public string Enviroment { get; set; }
        public string ExplorerKey { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public short Priority { get; set; }
        public bool Parallet { get; set; }
        public Set Set { get; set; }
        public IEnumerable<Parameter> Parameter { get; set; }
        private Dictionary<string, TestDataBase> _ParameterAsDictionary;
        public Dictionary<string, TestDataBase> ParameterAsDictionary
        {
            get
            { 
                if (_ParameterAsDictionary == null)
                {
                    _ParameterAsDictionary = new Dictionary<string, TestDataBase>();
                }
                if (_ParameterAsDictionary.Count == 0)
                {
                    LoadParameterAsDictionary();
                }
                return _ParameterAsDictionary;
            }
        }

        private void LoadParameterAsDictionary()
        {
            foreach(var param in Parameter.Where(w=> w.SetId == Set.Id))
            {
                var dataType = TestDataFactory.GetClass(param.Value, param.Key, (TestDataType)param.ParameterType);
                _ParameterAsDictionary.Add(param.Key, dataType);
            }
        }

    }
}
