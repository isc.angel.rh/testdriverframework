﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.BE.Model.TestCase
{
    public class TestCaseFilter
    {
        public TestCaseFilter() { }

        public TestCaseFilter (string App)
        {
            this.App = App;
        }

        public string App { get; set; }
        public bool ExecuteEnviroment { get { return true; } }
        public bool ExecuteSet { get { return true; } }
    }
}
