﻿using System;
using System.Collections.Generic;
using System.Text;
using TestCase.Model.Context;

namespace TestCase.BE.BE
{
    public class BaseBE
    {

        protected TestCaseContext db
        {
            get
            {
                return ConnectionManager.GetContext();
            }
        }
    }
}
