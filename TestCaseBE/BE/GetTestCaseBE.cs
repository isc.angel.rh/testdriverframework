﻿using System;
using System.Collections.Generic;
using System.Text;
using TestCase.BE.DA;
using TestCase.Model.Context;
using System.Linq;
using System.Data.Entity;
using TestCase.BE.Model.TestCase;
using TestCase.BE.DA.TestCase;

namespace TestCase.BE.BE
{
    public class GetTestCaseBE 
    {

        public GetListFilter<TestFromDb, TestCaseFilter> Da { get; set; }

        public GetTestCaseBE()
        {
            Da = new GetTestToExecute();
        }

        public IEnumerable<TestFromDb> Get(TestCaseFilter Filter)
        {
            var testDb = Da.Get(Filter);
            return testDb;
        }
    }
}
