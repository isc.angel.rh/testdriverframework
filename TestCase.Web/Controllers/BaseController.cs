﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestCase.BE.BE;
using TestCase.Model.Context;

namespace TestCase.Web.Controllers
{
    public class BaseController : Controller
    {
        protected readonly TestCaseContext _context;

        protected BaseBussinessEntity be;

        public BaseController()
        {
            be = new BaseBussinessEntity();
        }

        //public BaseController(TestCaseContext context)
        //{
        //    _context = context;
        //}

        // GET: Apps
        public async Task<IEnumerable<T>> Index<T>() where T : class
        {
            return await be.GetAllAsync<T>();
        }

        // GET: Apps/Details/5
        public async Task<IActionResult> Details<T>(int? id) where T : class
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await be.GetAsync<T>(id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // GET: ControllerName/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ControllerName/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create<T>(T item) where T:class
        {
            if (ModelState.IsValid)
            {
                await be.AddAsync<T>(item);
                return RedirectToAction(nameof(Index));
            }
            return View(item);
        }

        // GET: Apps/Edit/5
        public async Task<IActionResult> Edit<T>(int? id) where T: class
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await be.GetAsync<T>(id);// _context.App.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return View(item);
        }

        // POST: Apps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        protected async Task<IActionResult> Edit<T>(int id, T parameter, int parameterId) where T : class
        {
            if (id != parameterId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //var context = new TestCaseContext();
                    //context.Update(parameter);
                    //await context.SaveChangesAsync();
                    await be.UpdateAsync<T>(parameter);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists<T>(parameterId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(parameter);
            //return View(parameter);
        }

        //// GET: Apps/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var app = await _context.App
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (app == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(app);
        //}

        //// POST: Apps/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var app = await _context.App.FindAsync(id);
        //    _context.App.Remove(app);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        private bool ItemExists<T>(int? id) where T : class
        {
            return be.Exists<T>(id);
        }
    }
}
