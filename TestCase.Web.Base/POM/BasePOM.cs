﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace TestCase.Web.Base.POM
{
   

    public class BasePOM
    {
        public string BaseUrl { get; set; }
        protected string PartialUrl { get; set; }
        protected string FullUrl { get { return BaseUrl + PartialUrl; } }
        private Int32 Timeout = 10000; // in milliseconds
        protected IWebDriver Driver;
        protected WebDriverWait wait;



        [FindsBy(How = How.TagName, Using = "h1")]
        protected IWebElement H1;


        public BasePOM(IWebDriver driver)
        {
            this.Driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            PageFactory.InitElements(driver, this);
            BaseUrl = "";
            PartialUrl = "";
        }

        public void GoTo()
        {
            Driver.Navigate().GoToUrl(FullUrl);
            LoadIsComplete();
        }

        public void MaximizeWindow()
        {
            Driver.Manage().Window.Maximize();
        }

        public void GoTo(string Url)
        {
            Driver.Navigate().GoToUrl(Url);
            LoadIsComplete();
        }

        public string GetPageTitle()
        {
            return Driver.Title;
        }

        public string GetH1()
        {
            return H1.Text;
        }

        async void AsyncDelay()
        {
            await Task.Delay(50);
        }

        async void AsyncDelay(int miliseconds)
        {
            await Task.Delay(miliseconds);
        }

        public void WaitForElementVisible(By byElement)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromMilliseconds(Timeout * 3));
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(byElement));
        }

        public T OpenPageByClick<T>(IWebElement webElement)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromMilliseconds(Timeout));
            wait.Until(d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
            webElement.Click();
            AsyncDelay();
            return (T)Activator.CreateInstance(typeof(T), Driver);
        }

        public void LoadIsComplete()
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromMilliseconds(Timeout));
            wait.Until(d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public bool ElementExists(By by)
        {
            var elements = Driver.FindElements(by);
            return elements.Count != 0;
        }

        public void ScrollToElement(IWebElement element)
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element);
            actions.Perform();
        }
        public void Quit()
        {
            Driver.Quit();
        }
        public void Close()
        {
            Driver.Close();
        }

        public SelectElement AsDropDown(IWebElement webElement)
        {
            var dropDown = new SelectElement(webElement);
            return dropDown;
        }
    }
}
