﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public static class DriverManager
    {
        //private static IWebDriver DriverChrome;
        private static IWebDriver DriverFireFox;


        public static DriverType GetDriverType(string keyDriver)
        {
            switch (keyDriver)
            {
                case "CHR":
                    return DriverType.Chrome;
                case "FFOX":
                    return DriverType.FireFox;
                default:
                    throw new Exception("Driver not supported");
            }
        }

        public static IWebDriver GetDriverInstance(string Type)
        {
            switch (Type)
            {
                case "CHR":
                    return GetDriverInstance(DriverType.Chrome);
                case "FFOX":
                    return GetDriverInstance(DriverType.FireFox);
                default:
                    throw new Exception("Driver not supported");
            }
        }

        public static IWebDriver GetDriverInstance(DriverType Type)
        {
            switch (Type)
            {
                case DriverType.FireFox:
                    return GetFireFox();
                case DriverType.Chrome:
                    return GetChrome();
                default:
                    throw new Exception("Driver not supported");
            }
        }

        private static IWebDriver GetChrome()
        {
            //string pathToDrivers = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Resources";
            return new ChromeDriver(@"C:\selenium"); 
        }

        private static IWebDriver GetFireFox()
        {
            if (DriverFireFox == null)
                DriverFireFox = new FirefoxDriver(@"C:\Proyectos\testframework\testdriverframework\drivers\selenium-dotnet-3.14.0\dist");
            return DriverFireFox;
        }
    }
}
