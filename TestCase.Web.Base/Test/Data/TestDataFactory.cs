﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public static class TestDataFactory
    {
        public static TestDataBase GetClass(string Value, string Name, TestDataType Type)
        {
            TestDataBase result;
            switch (Type )
            {
                case TestDataType.Boolean:
                    result = new BoolTestData(Value, Name, Type);
                    break;
                case TestDataType.Decimal:
                    result = new DecimalTestData(Value, Name, Type);
                    break;
                case TestDataType.Integer:
                    result = new IntegerTestData(Value, Name, Type);
                    break;
                case TestDataType.String:
                case TestDataType.Null:
                default:
                    result = new StringTestData(Value, Name, Type);
                    break;
            }
            return result;
        }
    }
}
