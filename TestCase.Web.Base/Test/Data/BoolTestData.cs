﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public class BoolTestData : TestDataBase, GetTestDataValue<string>
    {
        public BoolTestData(string Value, string Name, TestDataType DataType) : base(Value, Name, DataType)
        {

        }

        public string GetValueX()
        {
            return base.Value;
        }
    }
}
