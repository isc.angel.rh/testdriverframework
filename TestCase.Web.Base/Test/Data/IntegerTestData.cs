﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public class IntegerTestData : TestDataBase, GetTestDataValue<int>
    {

        public IntegerTestData(string Value, string Name, TestDataType DataType) :base(Value, Name, DataType)
        {

        }

        public int GetValueX()
        {
            return Int32.Parse(base.Value);
        }

    }
}
