﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public abstract class TestDataBase
    {
        public string Value { get; set; }

        public string Name { get; set; }
        public TestDataType DataType { get; set; }

        public TestDataBase(string Value, string Name, TestDataType DataType)
        {
            this.Value = Value;
            this.Name = Name;
            this.DataType = DataType;
        }

    }
}
