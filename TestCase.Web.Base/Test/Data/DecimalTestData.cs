﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public class DecimalTestData : TestDataBase, GetTestDataValue<Decimal>
    {
        public DecimalTestData(string Value, string Name, TestDataType DataType) : base(Value, Name, DataType)
        {
             
        }

        public Decimal GetValueX()
        {
            return Decimal.Parse(base.Value);
        }
    }
}
