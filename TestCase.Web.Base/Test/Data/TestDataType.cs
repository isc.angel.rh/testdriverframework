﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public enum TestDataType
    {
        Null,
        Integer,
        Decimal,
        String,
        Boolean
    }
}
