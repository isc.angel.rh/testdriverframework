﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public interface GetTestDataValue<out R>
    {
        R GetValueX();
    }
}
