﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TestCase.Web.Base.Test.Builder
{
    public abstract class TestBase
    {
        protected IWebDriver Driver;
        protected string Enviroment;

        public string BaseUrl { get; set; }
        protected Dictionary<string, TestDataBase> Parameters { get; set; }

        public void SetBaseUrl(string BaseUrl)
        {
            this.BaseUrl = BaseUrl;
        }

        public void SetExplorer(DriverType Type)
        {
            Driver = DriverManager.GetDriverInstance(Type);
        }

        public void SetEnviroment(string Enviroment)
        {
            this.Enviroment = Enviroment;
        }

        public void SetParameters(Dictionary<string, TestDataBase> Parameters)
        {
            this.Parameters = Parameters;
        }

        public void SecuryFinish()
        {
            Driver.Quit();
        }

        public abstract void Prepare();

        public abstract void Execute();

        public abstract void EvaluateAsserts();

    }
}
