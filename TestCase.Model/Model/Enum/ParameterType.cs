﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Model.Model.Enum
{
    public enum ParameterType
    {
        stringType,
        boolType,
        intType
    }
}
