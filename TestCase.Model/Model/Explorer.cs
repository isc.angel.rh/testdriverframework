﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TestCase.Model.Model
{
    [Table("Explorer")]
    public class Explorer
    {
        [Key]
        public int Id { get; set; }
        [StringLength(20)]
        public string Key { get; set; }
        [StringLength(20)]
        public string Description { get; set; }
    }
}
