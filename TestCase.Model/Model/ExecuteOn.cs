﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TestCase.Model.Model
{
    [Table("ExecuteOn")]
    public class ExecuteOn
    {
        [Key]
        public int Id { get; set; }

        [Index("IX_ExecuteOn_TestCaseId_ExplorerId", 1, IsUnique = true)]
        public int TestCaseId { get; set; }
        [ForeignKey("TestCaseId")]
        public virtual TestCase TestCase { get; set; }

        [Index("IX_ExecuteOn_TestCaseId_ExplorerId", 1, IsUnique = true)]
        public int ExplorerId { get; set; }
        [ForeignKey("ExplorerId")]
        public virtual Explorer Explorer { get; set; }

        public bool Execute { get; set; }
    }
}
