﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TestCase.Model.Model
{
    [Table("Enviroment")]
    public class Enviroment
    {
        [Key]
        public int Id { get; set; }
        [StringLength(20)]
        public string Key { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        [StringLength(2000)]
        public string Link { get; set; }
        [StringLength(2000)]
        public string DataSource { get; set; }
    }
}
