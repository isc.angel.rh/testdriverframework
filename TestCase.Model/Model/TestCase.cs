﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace TestCase.Model.Model
{
    [Table("TestCase")]
    public class TestCase
    {
        [Key]
        public int Id { get; set; }

        public int AppId { get; set; }
        [ForeignKey("AppId")]
        public virtual App App { get; set; }

        public virtual List<Set> Sets { get; set; }

        [StringLength(100)]
        public string Key { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public short Priority { get; set; }
        
        public bool Parallet { get; set; }
    }
}
