﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TestCase.Model.Model
{
    [Table("TestCaseDependencies")]
    public class TestCaseDependencie
    {
        [Key]
        public int Id { get; set; }

        public int TestCaseId { get; set; }
        [ForeignKey("TestCaseId")]
        public virtual TestCase TestCase { get; set; }

        public int TestCaseDependencieId { get; set; }
        [ForeignKey("TestCaseDependencieId")]
        public virtual TestCase Dependencie { get; set; }

        //public short Priority { get; set; }
    }
}
