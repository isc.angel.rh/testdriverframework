﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TestCase.Model.Model
{
    [Table("App")]
    public class App
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Key { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
    }

}
