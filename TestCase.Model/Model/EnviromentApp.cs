﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Linq;
using TestCase.Model.Context;

namespace TestCase.Model.Model
{
    [Table("EnviromentApp")]
    public class EnviromentApp
    {
        [Key]
        public int Id { get; set; }

        [Index("IX_EnviromentApp_AppId_EnviromentId", 1, IsUnique = true)]
        [EnviromentAppUnitIndex]
        public int AppId { get; set; }
        [ForeignKey("AppId")]
        public virtual App App { get; set; }

        [Index("IX_EnviromentApp_AppId_EnviromentId", 1, IsUnique = true)]
        public int EnviromentId { get; set; }
        [ForeignKey("EnviromentId")]
        public virtual Enviroment Enviroment { get; set; }

        public bool Execute { get; set; }

    }


    public class EnviromentAppUnitIndex: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            using (var con = new ConnectionManager())
            {
                var entity = value as EnviromentApp;
                var db = ConnectionManager.GetContext();
                var entityDb = db.EnviromentApp.First(w => w.AppId == entity.AppId && w.EnviromentId == entity.EnviromentId);
                if (entityDb != null)
                {
                    return new ValidationResult("Already exists App/Enviroment combination.");
                }

            }
            return ValidationResult.Success;
        }
    }
}
