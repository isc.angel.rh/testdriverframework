﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace TestCase.Model.Model
{
    [Table("Set")]
    public class Set
    {
        [Key]
        public int Id { get; set; }

        [StringLength(20)]
        public string Key { get; set; }
        
        [StringLength(50)]
        public string Name { get; set; }
        
        [StringLength(150)]
        public string Description { get; set; }
        public bool Execute { get; set; }
        public int? TestCaseId { get; set; }
        [ForeignKey("TestCaseId")]
        public virtual TestCase TestCase { get; set; }
        public short Priority { get; set; }

        public virtual List<Parameter> Parameter { get; set; }
    }
}
