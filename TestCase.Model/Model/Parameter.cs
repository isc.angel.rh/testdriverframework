﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TestCase.Model.Model
{
    [Table("Parameter")]
    public class Parameter
    {

        [Key]
        public int Id { get; set; }

        public int SetId { get; set; }
        [ForeignKey("SetId")]
        public virtual Set Set { get; set; }

        [StringLength(20)]
        public string Key { get; set; }
        
        [StringLength(500)]
        public string Value { get; set; }

        [StringLength(500)]
        public string Decription { get; set; }

        public short ParameterType { get; set; }


    }
}
