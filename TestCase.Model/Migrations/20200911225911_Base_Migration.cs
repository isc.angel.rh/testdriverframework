﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestCase.Model.Migrations
{
    public partial class Base_Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "App",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(maxLength: 20, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enviroment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    Link = table.Column<string>(maxLength: 2000, nullable: true),
                    DataSource = table.Column<string>(maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enviroment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Explorer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Explorer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TestCase",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppId = table.Column<int>(nullable: false),
                    Key = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Priority = table.Column<short>(nullable: false),
                    Parallet = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestCase", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TestCase_App_AppId",
                        column: x => x.AppId,
                        principalTable: "App",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EnviromentApp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppId = table.Column<int>(nullable: false),
                    EnviromentId = table.Column<int>(nullable: false),
                    Execute = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnviromentApp", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnviromentApp_App_AppId",
                        column: x => x.AppId,
                        principalTable: "App",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EnviromentApp_Enviroment_EnviromentId",
                        column: x => x.EnviromentId,
                        principalTable: "Enviroment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExecuteOn",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TestCaseId = table.Column<int>(nullable: false),
                    ExplorerId = table.Column<int>(nullable: false),
                    Execute = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecuteOn", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExecuteOn_Explorer_ExplorerId",
                        column: x => x.ExplorerId,
                        principalTable: "Explorer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecuteOn_TestCase_TestCaseId",
                        column: x => x.TestCaseId,
                        principalTable: "TestCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Set",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(maxLength: 20, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 150, nullable: true),
                    Execute = table.Column<bool>(nullable: false),
                    TestCaseId = table.Column<int>(nullable: true),
                    Priority = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Set", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Set_TestCase_TestCaseId",
                        column: x => x.TestCaseId,
                        principalTable: "TestCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TestCaseDependencies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TestCaseId = table.Column<int>(nullable: false),
                    TestCaseDependencieId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestCaseDependencies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TestCaseDependencies_TestCase_TestCaseDependencieId",
                        column: x => x.TestCaseDependencieId,
                        principalTable: "TestCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestCaseDependencies_TestCase_TestCaseId",
                        column: x => x.TestCaseId,
                        principalTable: "TestCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Parameter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SetId = table.Column<int>(nullable: false),
                    Key = table.Column<string>(maxLength: 20, nullable: true),
                    Value = table.Column<string>(maxLength: 500, nullable: true),
                    Decription = table.Column<string>(maxLength: 500, nullable: true),
                    ParameterType = table.Column<short>(nullable:false,defaultValue:0)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parameter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Parameter_Set_SetId",
                        column: x => x.SetId,
                        principalTable: "Set",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EnviromentApp_AppId",
                table: "EnviromentApp",
                column: "AppId");

            migrationBuilder.CreateIndex(
                name: "IX_EnviromentApp_EnviromentId",
                table: "EnviromentApp",
                column: "EnviromentId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecuteOn_ExplorerId",
                table: "ExecuteOn",
                column: "ExplorerId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecuteOn_TestCaseId",
                table: "ExecuteOn",
                column: "TestCaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Parameter_SetId",
                table: "Parameter",
                column: "SetId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_TestCaseId",
                table: "Set",
                column: "TestCaseId");

            migrationBuilder.CreateIndex(
                name: "IX_TestCase_AppId",
                table: "TestCase",
                column: "AppId");

            migrationBuilder.CreateIndex(
                name: "IX_TestCaseDependencies_TestCaseDependencieId",
                table: "TestCaseDependencies",
                column: "TestCaseDependencieId");

            migrationBuilder.CreateIndex(
                name: "IX_TestCaseDependencies_TestCaseId",
                table: "TestCaseDependencies",
                column: "TestCaseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnviromentApp");

            migrationBuilder.DropTable(
                name: "ExecuteOn");

            migrationBuilder.DropTable(
                name: "Parameter");

            migrationBuilder.DropTable(
                name: "TestCaseDependencies");

            migrationBuilder.DropTable(
                name: "Enviroment");

            migrationBuilder.DropTable(
                name: "Explorer");

            migrationBuilder.DropTable(
                name: "Set");

            migrationBuilder.DropTable(
                name: "TestCase");

            migrationBuilder.DropTable(
                name: "App");
        }
    }
}
