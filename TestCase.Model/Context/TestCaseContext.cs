﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using TestCase.Model.Model;

namespace TestCase.Model.Context
{
    public class TestCaseContext : DbContext
    {

        private readonly string connectionString;
        public TestCaseContext(DbContextOptions<TestCaseContext> options) : base(options)
        {
        }

        public TestCaseContext(string connectionString) : base()
        {
            this.connectionString = connectionString;
        }

        public TestCaseContext() : base()
        {
            var builder = new ConfigurationBuilder();
            //

            //System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName


            //builder.AddJsonFile("appsettings.json", optional: false);
            builder.AddJsonFile(@"C:\Proyectos\testframework\testdriverframework\TestCase.Web\appsettings.json", optional: false);
            var configuration = builder.Build();
            connectionString = configuration.GetConnectionString("TestCaseConection").ToString();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        public virtual DbSet<App> App { get; set; }
        public virtual DbSet<Enviroment> Environment { get; set; }
        public virtual DbSet<EnviromentApp> EnviromentApp { get; set; }
        public virtual DbSet<ExecuteOn> ExecuteOn { get; set; }
        public virtual DbSet<Explorer> Explorer { get; set; }
        public virtual DbSet<Parameter> Parameter { get; set; }
        public virtual DbSet<Set> Set { get; set; }
        public virtual DbSet<TestCase.Model.Model.TestCase> TestCase { get; set; }
        public virtual DbSet<TestCaseDependencie> TestCaseDependencie { get; set; }
    }
}
