﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.Model.Context
{
    public class ConnectionManager: IDisposable
    {
        private static TestCaseContext db;

        private static bool dispose = true;
        public static TestCaseContext GetContext()
        {
            if (db == null)
            {
                db = new TestCaseContext();
                dispose = false;
            }
            return db;
        }
        
        public void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
                dispose = true;
            }
        }
    }
}
