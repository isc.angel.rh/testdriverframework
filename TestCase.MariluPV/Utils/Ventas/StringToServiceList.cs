﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestCase.MariluPV.Model.Servicio;

namespace TestCase.MariluPV.Utils.Ventas
{
    public class StringToServiceList
    {
        public List<ServicioVenta>GetList(string servicios)
        {
            var result = new List<ServicioVenta>();

            if (!string.IsNullOrEmpty(servicios))
            {
                //"Lavadora Grande:5.0|"
                var serviciosList = servicios.Split("|").ToList();

                foreach(var servicio in serviciosList)
                {
                    if (!string.IsNullOrEmpty(servicio))
                    {
                        var servicioParts = servicio.Split(":");
                        if (servicioParts.Count() > 1)
                        {
                            result.Add(new ServicioVenta()
                            {
                                Servicio = servicioParts[0],
                                Cantidad= double.Parse( servicioParts[1])
                            });
                        }
                    }
                }

            }

            return result;
        }
    }
}
