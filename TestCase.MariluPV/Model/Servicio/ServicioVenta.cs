﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.MariluPV.Model.Servicio
{
    public class ServicioVenta
    {
        public string Servicio { get; set; }
        public double Cantidad { get; set; }
    }
}
