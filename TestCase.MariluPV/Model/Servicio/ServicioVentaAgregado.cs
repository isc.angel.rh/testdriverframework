﻿using NUnit.Framework.Constraints;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestCase.MariluPV.Model.Servicio
{
    public class ServicioVentaAgregado
    {
        public string Servicio { get; set; }
        public string Clave { get; set; }
        public string Unidades { get; set; }
        public double Costo { get; set; }
        public double Cantidad { get; set; }
        public double SubTotal { get; set; }
    }
}
