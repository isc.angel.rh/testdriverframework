using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using TestCase.MariluPV.Test;
using TestCase.Web.Base.Test.Builder;

namespace TestCase.MariluPV
{
    public class TestCaseMariluPV
    {

        [OneTimeSetUp]
        public void OneTimeConfig()
        {
            //restore DB oneTime
        }

        [SetUp]
        
        public void Setup()
        {
        }

        [TestCaseSource(sourceName: "TestSource")]
        public void BaseTest(TestStructure test)
        {
            test.Test();
        }

        public static IEnumerable<TestCaseData> TestSource
        {
            get
            {
                var testCases = new Generator().GetTestCaseData();
                foreach (var testCase in testCases)
                    yield return testCase;
            }
        }

    }
}