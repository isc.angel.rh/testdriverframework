﻿using System;
using System.Collections.Generic;
using System.Text;
using TestCase.Web.Base.Test.Builder;

namespace TestCase.MariluPV.Test
{
    public static class TestCaseFactory
    {
        private static string NAME_SPACE = "TestCase.MariluPV.Test.Execute.";
        public static TestBase Get(string Test)
        {
            TestBase result = null; ;
            try
            {
                Type t = Type.GetType(NAME_SPACE + Test);
                result = (TestBase)Activator.CreateInstance(t);
            }
            catch
            {
                throw new Exception("Test case is not implemented");
            }
            return result;
        }
    }
}
