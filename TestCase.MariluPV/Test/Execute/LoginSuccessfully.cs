﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TestCase.MariluPV.POM;
using TestCase.Web.Base.Test.Builder;

namespace TestCase.MariluPV.Test.Execute
{
    public class LoginSuccessfully : TestBase
    {

        public Main Main { get; set; }
        public MainLogIn MainLogin { get; set; }

        public string User { get; set; }
        public string Password { get; set; }

        public override void Prepare()
        {
            Main = new Main(Driver);
            Main.BaseUrl = BaseUrl;
            User = Parameters["user"].Value;
            Password = Parameters["password"].Value;
        }
        public override void Execute()
        {
            Main.GoTo();
            var login = Main.GoToLogin();
            login.SetEmail(User);
            login.SetPassword(Password);
            MainLogin = (MainLogIn)login.TryToLogIn();
        }

        public override void EvaluateAsserts()
        {
            Assert.AreEqual("ASP.NET", MainLogin.GetH1());
        }

    }
}
