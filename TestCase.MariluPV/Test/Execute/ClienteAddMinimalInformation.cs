﻿using NUnit.Framework;
using TestCase.MariluPV.POM;

namespace TestCase.MariluPV.Test.Execute
{
    public class ClienteAddMinimalInformation : ExecuteBase
    {

        public Cliente Cliente { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string ListaPrecio { get; set; }

        public override void Prepare()
        {
            base.Prepare();
            Nombre = Parameters["nombre"].Value;
            Apellidos = Parameters["apellidos"].Value;
            ListaPrecio = Parameters["listaPrecio"].Value;
        }

        public override void Execute()
        {
            base.Execute();
            SetNewCliente();
            Cliente.WaitForAlert();
            Cliente.CloseMessageAlert();
            OpenDetailNewCliente();
        }

        private void SetNewCliente()
        {
            Cliente = MainLogin.GoToCliente();
            Cliente.ClickNuevo();
            Cliente.SetNombre(Nombre);
            Cliente.SetApellidos(Apellidos);
            Cliente.SetPrecios(ListaPrecio);
            Cliente.ClickOk();
        }

        private void OpenDetailNewCliente()
        {
            Cliente.SetSearch(Nombre + " " + Apellidos);
            Cliente.ClickEditByNombre(Nombre);
        }

        public override void EvaluateAsserts()
        {
            Assert.AreEqual(Nombre, Cliente.GetNombre());
            Assert.AreEqual(Apellidos, Cliente.GetApellidos());
            Assert.AreEqual("", Cliente.GetTelefono());
            Assert.AreEqual("", Cliente.GetEmail());
            Assert.AreEqual(ListaPrecio, Cliente.GetListaPrecios());
        }
    }
}
