﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TestCase.MariluPV.POM;
using TestCase.Web.Base.Test.Builder;

namespace TestCase.MariluPV.Test.Execute
{
    public class LoginSuccessFail : TestBase
    {
        public Main Main { get; set; }
        public LogIn LogIn { get; set; }

        public override void Prepare()
        {
            Main = new Main(Driver);
            Main.BaseUrl = BaseUrl;
        }
        public override void Execute()
        {
            Main.GoTo();
            var login = Main.GoToLogin();
            login.SetEmail(Parameters["user"].Value);
            login.SetPassword(Parameters["password"].Value);
            LogIn = (LogIn)login.TryToLogIn();
        }
        public override void EvaluateAsserts()
        {
            Assert.AreEqual("Log in - MariluPV", LogIn.GetPageTitle());
        }

    }
}
