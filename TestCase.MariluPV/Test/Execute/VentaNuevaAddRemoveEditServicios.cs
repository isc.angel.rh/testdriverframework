﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using TestCase.MariluPV.Model.Servicio;
using TestCase.MariluPV.POM;
using TestCase.MariluPV.Utils.Ventas;

namespace TestCase.MariluPV.Test.Execute
{
    public class VentaNuevaAddRemoveEditServicios : ExecuteBase
    {
        public VentasHoy VentasHoy { get; set; }
        public VentaNueva VentaNueva { get; set; }
        public List<ServicioVenta> Servicios { get; set; }


        public override void Prepare()
        {
            StringToServiceList parseServicios = new StringToServiceList();
            base.Prepare();
            Servicios = parseServicios.GetList(Parameters["servicios"].Value);
        }

        public override void Execute()
        {
            base.Execute();
            VentasHoy = MainLogin.GoToVentasHoy();
            Assert.AreEqual("Ventas abiertas", VentasHoy.GetH1());
            VentaNueva = VentasHoy.ClickNuevo();
            Assert.AreEqual("Nueva Venta", VentaNueva.GetH1());

            AddServices();
            EditService();
            RemoveAddedService();
        }

        private void AddServices()
        {
            foreach (var servicio in Servicios)
            {
                AddServices(servicio);
            }
            VerifyTotal();
        }

        private void AddServices(ServicioVenta servicio)
        {
            VentaNueva.AddServicio(servicio.Servicio, servicio.Cantidad);
            var serviciosAgregados = VentaNueva.GetServicioAgregados();
            var expected = serviciosAgregados.Find(f => f.Servicio.Equals(servicio.Servicio));
            Assert.Contains(expected, serviciosAgregados);
            VentaNueva.SetSearchServiciosAdd(servicio.Servicio);
            Assert.IsFalse(VentaNueva.ServiceIsAvailableToAdd(servicio.Servicio));
        }

        private void EditService()
        {
            VentaNueva.EditAddedServicio(Servicios[1].Servicio, Servicios[1].Cantidad + 1);
            VerifyTotal();
        }

        private void RemoveAddedService()
        {
            VentaNueva.RemoveAddedServicio(Servicios[2].Servicio);
            VerifyTotal();
        }

        public void VerifyTotal()
        {
            var serviciosAgregados = VentaNueva.GetServicioAgregados();
            var expected = serviciosAgregados.Sum(s => s.SubTotal);
            Assert.AreEqual(expected, double.Parse( VentaNueva.GetTotal()));
        }

        public override void EvaluateAsserts()
        {
            
        }
    }
}
