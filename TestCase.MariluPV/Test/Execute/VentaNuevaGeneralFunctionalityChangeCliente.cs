﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TestCase.MariluPV.Model.Servicio;
using TestCase.MariluPV.POM;
using TestCase.MariluPV.Utils.Ventas;

namespace TestCase.MariluPV.Test.Execute
{
    public class VentaNuevaGeneralFunctionalityChangeCliente : ExecuteBase
    {
        public VentasHoy VentasHoy { get; set; }
        public VentaNueva VentaNueva { get; set; }
        public string NombreCliente { get; set; }
        public string ApellidosCliente { get; set; }
        public string NombreClienteUnico { get; set; }
        public List<ServicioVenta> Servicios { get; set; }
        

        public override void Prepare()
        {
            StringToServiceList parseServicios = new StringToServiceList();
            base.Prepare();
            NombreCliente = Parameters["nombreCliente"].Value;
            ApellidosCliente = Parameters["apellidosCliente"].Value;
            NombreClienteUnico = Parameters["nombreClienteUnico"].Value;
            Servicios = parseServicios.GetList( Parameters["servicios"].Value);
        }

        public override void Execute()
        {
            base.Execute();
            VentasHoy = MainLogin.GoToVentasHoy();
            Assert.AreEqual("Ventas abiertas",VentasHoy.GetH1());
            VentaNueva = VentasHoy.ClickNuevo();
            Assert.AreEqual("Nueva Venta", VentaNueva.GetH1());
            Assert.AreEqual("Cliente Unico NA", VentaNueva.GetClienteSelected());
            VentaNueva.AddServicio(Servicios[0].Servicio, Servicios[0].Cantidad);
            Assert.IsTrue(VentaNueva.ServiceIsAdded(Servicios[0].Servicio));
            VentaNueva.ScrollToGuardar();
            Assert.AreEqual("380", VentaNueva.GetTotal());
            VentaNueva.ScrollToTop();
            VentaNueva.ClickBuscarCliente();
            VentaNueva.WaitForListClientes();
            VentaNueva.SetBuscarClienteSearch(NombreCliente, ApellidosCliente);
            VentaNueva.SelectCliente(NombreCliente);
            Assert.AreEqual("Javier Jaloma", VentaNueva.GetClienteSelected());
            VentaNueva.ScrollToGuardar();
            Assert.AreEqual("0", VentaNueva.GetTotal());
            VentasHoy = VentaNueva.ClickRegresarButton();
            VentaNueva = VentasHoy.ClickNuevo();
        }

        public override void EvaluateAsserts()
        {
            Assert.AreEqual("Cliente Unico NA", VentaNueva.GetClienteSelected());
        }
    }
}
