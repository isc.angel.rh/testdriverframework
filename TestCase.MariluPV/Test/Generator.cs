﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using TestCase.BE.BE;
using TestCase.BE.Model.TestCase;
using TestCase.Web.Base.Test.Builder;

namespace TestCase.MariluPV.Test
{
    public class Generator
    {
        
        public string App { get { return "MPV"; } }

        public IEnumerable<TestCaseData> GetTestCaseData()
        {
            var result = new List<TestCaseData>();
            var testDb = GetTestFromExecute();
            foreach (var test in testDb)
            {
                TestBase testToExecute = GetTestToExecute(test, test);
                var tc = WrapIntoTestCaseData(testToExecute, test);
                result.Add(tc);
            }
            return result;
        }

        private IEnumerable<TestFromDb> GetTestFromExecute()
        {
            var be = new GetTestCaseBE();
            var testDb = be.Get(new TestCaseFilter() { App = App });
            return testDb;
        }

        private static TestBase GetTestToExecute(TestFromDb test, TestFromDb testDb)
        {
            var testToExecute = TestCaseFactory.Get(test.Key);
            return testToExecute;
        }

        private TestCaseData WrapIntoTestCaseData(TestBase testToExecute, TestFromDb testDb)
        {
            var tc = new TestCaseData(
                new TestStructure
                {
                    Test = () =>
                    {
                        try
                        {
                            testToExecute.SetBaseUrl(testDb.Link);
                            testToExecute.SetExplorer(DriverManager.GetDriverType(testDb.ExplorerKey));
                            testToExecute.SetParameters(testDb.ParameterAsDictionary);
                            testToExecute.Prepare();
                            testToExecute.Execute();
                            testToExecute.EvaluateAsserts();
                        }
                        catch(Exception ex)
                        {
                            throw new Exception(testDb.Name  + " Fail",ex);
                        }
                        finally
                        {
                            testToExecute.SecuryFinish();
                        }
                    }
                }).SetName(testDb.Name)
                .SetDescription(testDb.Description);
            return tc;
        }

    }
}
