﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using TestCase.Web.Base.POM;

namespace TestCase.MariluPV.POM
{
    public class LogIn : BasePOM
    {
        [FindsBy(How = How.Id, Using = "Email")]
        private IWebElement Email;

        [FindsBy(How = How.Id, Using = "Password")]
        private IWebElement Password;

        [FindsBy(How = How.XPath, Using = "//input[@value='Log in']")]
        private IWebElement LogInButton;

        [FindsBy(How = How.XPath, Using = "//h2")]
        private IWebElement HeaderH2;

        public LogIn(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void SetEmail(string email)
        {
            Email.SendKeys(email);
        }

        public void SetPassword(string password)
        {
            Password.SendKeys(password);
        }

        public BasePOM TryToLogIn()
        {
            MainLogIn result = OpenPageByClick<MainLogIn>(LogInButton);
            if (ElementExists(By.XPath("//h2[contains(text(),'Log in')]")))
            {
                return this;
            }
            return result;
        }

    }
}
