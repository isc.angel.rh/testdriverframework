﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using TestCase.Web.Base.POM;

namespace TestCase.MariluPV.POM
{
    
    public class Main : BasePOM
    {

        [FindsBy(How = How.Id, Using = "loginLink")]
        private IWebElement LogInLink;

        public Main(IWebDriver Driver) : base(Driver)
        {
            PageFactory.InitElements(Driver, this);
        }

        public LogIn GoToLogin()
        {
            LogIn logIn = OpenPageByClick<LogIn>(LogInLink);
            return logIn;
        }

    }
}
