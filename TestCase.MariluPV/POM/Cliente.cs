﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Linq;
using TestCase.Web.Base.POM;

namespace TestCase.MariluPV.POM
{
    public class Cliente : MainLogIn
    {

        [FindsBy(How = How.XPath, Using = "//a[text()='Nuevo']")]
        private IWebElement Nuevo;
        [FindsBy(How = How.XPath, Using = "//input[@type='search']")]
        private IWebElement Search;

        #region Capture
        [FindsBy(How = How.TagName, Using = "h4")]
        private IWebElement HeaderCaptura;
        [FindsBy(How = How.Name, Using = "Nombre")]
        private IWebElement Nombre;
        [FindsBy(How = How.Name, Using = "Apellidos")]
        private IWebElement Apellidos;
        [FindsBy(How = How.Name, Using = "Email")]
        private IWebElement Email;
        [FindsBy(How = How.Name, Using = "Telefono")]
        private IWebElement Telefono;
        [FindsBy(How = How.XPath, Using = "//select[@ng-model='Precios']")]
        private IWebElement Precios;
        [FindsBy(How = How.XPath, Using = "//button[text()='OK']")]
        private IWebElement Ok;
        [FindsBy(How = How.XPath, Using = "//button[text()='Cancel']")]
        private IWebElement Cancel;
        #endregion

        public Cliente(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void ClickNuevo()
        {
            Nuevo.Click();
        }

        public void SetSearch(string text)
        {
            Search.SendKeys(text);
        }

        public void ClickEditByNombre(string nombre)
        {
            var edit = Driver.FindElements(By.XPath("//tbody//td[contains(text(),'" + nombre + "')]//following::button")).First();
            edit.Click();
        }

        public void SetNombre(string nombre)
        {
            Nombre.SendKeys(nombre);
        }
        public void SetApellidos(string apellidos)
        {
            Apellidos.SendKeys(apellidos);
        }
        public void SetEmail(string email)
        {
            Email.SendKeys(email);
        }
        public void SetTelefono(string telefono)
        {
            Telefono.SendKeys(telefono);
        }
        public void SetPrecios(string precio)
        {
            var preciosList = AsDropDown(Precios);
            preciosList.SelectByText(precio);
        }

        public void ClickOk()
        {
            Ok.Click();
        }
        public void ClickCancel()
        {
            Cancel.Click();
        }

        public string GetNombre()
        {
            return Nombre.GetAttribute("value");
        }
        public string GetApellidos()
        {
            return Apellidos.GetAttribute("value");
        }
        public string GetTelefono()
        {
            return Telefono.GetAttribute("value");
        }
        public string GetEmail()
        {
            return Email.GetAttribute("value");
        }
        public string GetListaPrecios()
        {
            var preciosList = AsDropDown(Precios);
            var text = preciosList.SelectedOption.Text;
            return text;
        }

    }
}
