﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestCase.MariluPV.Model.Servicio;

namespace TestCase.MariluPV.POM
{
    public class VentaNueva : MainLogIn
    {

        [FindsBy(How = How.TagName, Using = "h1")]
        private IWebElement H1;

        [FindsBy(How = How.XPath, Using = "//label[text()='Cliente:']//following::label")]
        private IWebElement ClienteSelected;

        [FindsBy(How = How.XPath, Using = "(//a[text()='Regresar'])[1]")]
        private IWebElement RegresarTop;
        [FindsBy(How = How.XPath, Using = "(//a[text()='Regresar'])[2]")]
        private IWebElement RegresarButton;

        [FindsBy(How = How.XPath, Using = "//a[text()='Bucar Cliente']")]
        private IWebElement BuscarCliente;
        [FindsBy(How = How.XPath, Using = "//div[@id='DataTables_Table_0_filter']//input[@type='search']")]
        private IWebElement BuscarClienteSearch;
        private By BuscarClienteTableFoter = By.XPath("//div[@class='main-box-body clearfix']//div[contains(text(),'Showing 1 to 10')]");

        [FindsBy(How = How.XPath, Using = "//input[@type='search']")]
        private IWebElement SearchProductosAdd;
        [FindsBy(How = How.XPath, Using = "//input[@name='SrvSeleccionado']")]
        private IWebElement ServicioSeleccionadoDescripcion;
        [FindsBy(How = How.XPath, Using = "//input[@name='Cantidad']")]
        private IWebElement ServicioSeleccionadoCantidad;
        [FindsBy(How = How.XPath, Using = "//button[@data-ng-click='add()']")]
        private IWebElement ServicioAdd;
        [FindsBy(How = How.XPath, Using = "//button[@data-ng-click='cancelAdd()']")]
        private IWebElement ServicioCancelAdd;
        [FindsBy(How = How.XPath, Using = "//a[text()='Guardar']")]
        private IWebElement Guardar;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Servicios agregados']//following::table//tbody")]
        private IWebElement ServiciosAgregados;

        [FindsBy(How = How.Name, Using = "Anticipos")]
        private IWebElement Anticipos;
        [FindsBy(How = How.Name, Using = "Abono")]
        private IWebElement Abono;
        [FindsBy(How = How.Name, Using = "PorPagar")]
        private IWebElement PorPagar;
        [FindsBy(How = How.Name, Using = "Total")]
        private IWebElement Total;
        [FindsBy(How = How.Name, Using = "Comentario")]
        private IWebElement Comentario;

        private List<ServicioVentaAgregado> ServicioAgregados { get; set; }

        public VentaNueva(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void SetSearchServiciosAdd(string servicio)
        {
            SearchProductosAdd.Clear();
            SearchProductosAdd.SendKeys(servicio);
        }

        public void SetBuscarClienteSearch(string nombre, string apellidos)
        {
            var fullName = (string.IsNullOrEmpty(nombre) ? "" : nombre) + " " + (string.IsNullOrEmpty(apellidos) ? "" : apellidos);
            BuscarClienteSearch.Clear();
            BuscarClienteSearch.SendKeys(fullName);
        }

        public void SelectCliente(string nombre)
        {
            var select = Driver.FindElements(By.XPath("//div[@class='main-box-body clearfix']//tr//td[text()='"+ nombre+"']//following::button")).First();
            select.Click();
        }

        public bool ServiceIsAvailableToAdd(string servicio)
        {
            var serviceAvailables = Driver.FindElements(By.XPath("//table[@role='grid']//td[text()='" + servicio + "']//following::button"));
            return serviceAvailables.Count > 0;
        }

        public void SelectServicioAdd(string servicio)
        {
            var select = Driver.FindElements(By.XPath("//table[@role='grid']//td[text()='" + servicio + "']//following::button")).First();
            select.Click();
        }

        public bool ServiceIsAdded(string servicio)
        {
            var serviceAdded = Driver.FindElements(By.XPath("//h2[text()='Servicios agregados']//following::table//tr/td[text()='" + servicio + "']"));
            return serviceAdded.Count > 0;
        }

        public void WaitForListClientes()
        {
            WaitForElementVisible(BuscarClienteTableFoter);
        }

        public void ScrollToTop()
        {
            ScrollToElement(RegresarTop);
        }

        public void ScrollToGuardar()
        {
            ScrollToElement(Guardar);
        }

        public string GetTotal()
        {
            return Total.GetAttribute("value");
        }

        public void ClickBuscarCliente()
        {
            BuscarCliente.Click();
        }

        public VentasHoy ClickRegresarTop()
        {
            VentasHoy result = OpenPageByClick<VentasHoy>(RegresarTop);
            return result;
        }
        public VentasHoy ClickRegresarButton()
        {
            VentasHoy result = OpenPageByClick<VentasHoy>(RegresarButton);
            return result;
        }

        public string GetClienteSelected()
        {
            var cliente = ClienteSelected.Text;
            return cliente;
        }

        public void AddServicio(string servicio, double cantidad)
        {
            SearchProductosAdd.Clear();
            SearchProductosAdd.SendKeys(servicio);
            SelectServicioAdd(servicio);
            ServicioSeleccionadoCantidad.Clear();
            ServicioSeleccionadoCantidad.SendKeys(cantidad.ToString());
            ServicioAdd.Click();
        }

        public void EditAddedServicio(string servicio, double cantidad)
        {
            var serviceAdded = Driver.FindElements(By.XPath("//h2[text()='Servicios agregados']//following::table//tr/td[text()='" + servicio + "']//following::button[1]")).FirstOrDefault();
            if (serviceAdded != null)
            {
                serviceAdded.Click();
                ServicioSeleccionadoCantidad.Clear();
                ServicioSeleccionadoCantidad.SendKeys(cantidad.ToString());
                ServicioAdd.Click();
            }
        }
        public void RemoveAddedServicio(string servicio)
        {
            var serviceAdded = Driver.FindElements(By.XPath("//h2[text()='Servicios agregados']//following::table//tr/td[text()='" + servicio + "']//following::button[2]")).FirstOrDefault();
            if (serviceAdded != null)
            {
                serviceAdded.Click();
            }
        }

        private void LoadServiciosSeleccionados()
        {
            ServicioAgregados = new List<ServicioVentaAgregado>();
            var records = ServiciosAgregados.FindElements(By.TagName("tr"));
            foreach(var record in records)
            {
                var columns = record.FindElements(By.TagName("td"));
                var add = new ServicioVentaAgregado()
                {
                    Servicio = columns[0].Text,
                    Clave = columns[1].Text,
                    Unidades = columns[2].Text,
                    Costo = double.Parse(columns[3].Text),
                    Cantidad = double.Parse(columns[4].Text),
                    SubTotal = double.Parse(columns[5].Text),
                };
                ServicioAgregados.Add(add);
            }
        }

        public List<ServicioVentaAgregado>  GetServicioAgregados()
        {
            LoadServiciosSeleccionados();
            return ServicioAgregados;
        }
    }
}
