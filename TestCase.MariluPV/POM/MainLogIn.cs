﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using TestCase.Web.Base.POM;

namespace TestCase.MariluPV.POM
{
    public class MainLogIn : BasePOM
    {

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Home')]")]
        private IWebElement HomeOption;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Catalogos')]")]
        private IWebElement CatalogOption;
        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Cliente')]")]
        private IWebElement ClienteOption;


        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Ventas')]")]
        private IWebElement VentasOption;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Nueva')]")]
        private IWebElement HoyOption;



        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Log off')]")]
        private IWebElement LogOffOption;

        [FindsBy(How = How.XPath, Using = "//div[@id='alertWindow']//button")]
        private IWebElement CloseAlert;



        [FindsBy(How = How.XPath, Using = "//a[text()='Nuevo']")]
        protected IWebElement Nuevo;
        [FindsBy(How = How.XPath, Using = "//input[@type='search']")]
        protected IWebElement Search;

        public MainLogIn(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public Main LogOff()
        {
            Main result = OpenPageByClick<Main>(LogOffOption);
            return result;
        }

        public bool IsLogin()
        {
            return LogOffOption.Displayed;
        }

        public void ClickCatalogs()
        {
            CatalogOption.Click();
        }

        public void WaitForAlert()
        {
            WaitForElementVisible(By.XPath("//div[@id='alertWindow']//button"));
        }

        public void CloseMessageAlert()
        {
            CloseAlert.Click();
        }

        public Cliente GoToCliente()
        {
            CatalogOption.Click();
            Cliente result = OpenPageByClick<Cliente>(ClienteOption);
            return result;
        }
        public VentasHoy GoToVentasHoy()
        {
            VentasOption.Click();
            VentasHoy result = OpenPageByClick<VentasHoy>(HoyOption);
            return result;
        }


    }
}
