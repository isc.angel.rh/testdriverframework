﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestCase.MariluPV.POM
{
    public class VentasHoy : MainLogIn
    {

        [FindsBy(How = How.XPath, Using = "//a[@href='/Venta/Nueva/id=/#']")]
        private IWebElement Nuevo;

        [FindsBy(How = How.TagName, Using = "h1")]
        private IWebElement H1;

        public VentasHoy(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public VentaNueva ClickNuevo()
        {
            VentaNueva result = OpenPageByClick<VentaNueva>(Nuevo);
            return result;
        }

        public void SetSearch(string text)
        {
            Search.SendKeys(text);
        }

        public void ClickEditByFolio(string folio)
        {
            var edit = Driver.FindElements(By.XPath("//tbody//td[contains(text(),'" + folio + "')]//following::button")).First();
            edit.Click();
        }

    }
}
